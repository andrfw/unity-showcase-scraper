package main

import (
  "fmt"
  "net/http"
  "strings"
  "strconv"
  "io/ioutil"
  "sync"
  "time"
)

var userVisits map[string]int
var mutex = &sync.Mutex{}
var pages []string

func main() {
  scrape()
  go scrapeDaily()

  userVisits = make(map[string]int)
  http.HandleFunc("/unity/", sendWebpage)
  err := http.ListenAndServe(":8080", nil)
  checkErr(err)
}

// Writes a new game webpage out to the given writer 
// The webpage will be one that has not been seen
// before by the global IP address given in the request.
// If the IP has seen all games, they will be given a 
// game that they have seen before.
//
// prints the number of times this user has seen the webpage
// to the console.
//
// If there are no webpages currently stored, it will print
// the message "The server is down right now, please try again later!"
// to the user's browser.
func sendWebpage(writer http.ResponseWriter, request *http.Request) {
  ip := strings.Split(request.RemoteAddr, ":")[0]
  _, userExists := userVisits[ip]
  if userExists {
    userVisits[ip]++
  } else {
    userVisits[ip] = 0
  }

  mutex.Lock()
  if (len(pages) > 0) {
    fmt.Fprint(writer, pages[userVisits[ip] % len(pages)])
  } else {
    fmt.Fprint(writer, "The server is down right now, please try again later!")
  }
  mutex.Unlock()

  fmt.Println(ip + " - " + strconv.Itoa(userVisits[ip]))
}

// returns the HTML source for the website given by the url
// as a string. Panics if the HTTP Get request for the URL
// fails or if the retrieved data is unreadable.
func getSource(url string) string {
  resp, err := http.Get(url)
  checkErr(err)
  html, err := ioutil.ReadAll(resp.Body)
  checkErr(err)
  return string(html)
}

// calls scrape every 24 hours starting 24 hours after this
// function is called.
func scrapeDaily() {
  for {
    time.Sleep(time.Hour * 24)
    scrape()
  }
}

// Scrapes the unity showcase database for all games and non-games 
// fills the pages array with a webpage with the information from each
//
// prints "Scraping..." to the console
func scrape() {
  fmt.Println("Scraping...")

  pageString := getSource("https://unity3d.com//showcase/gallery/more/Default/games/weight/-1/0/all/all/group")
  pageString += getSource("https://unity3d.com//showcase/gallery/more/Default/non-games/weight/-1/0/all/all/group")

  mutex.Lock()
  pages = getGameList(pageString)
  mutex.Unlock()
}

// Returns a list of simple webpages containing the information of all the games described
// in the HTML game list(s) provided in the HTML parameter.
// For more information about the formatting of the simple webpages, see "makeWebPage"
// Panics if the given html is malformed (ie, if it is not in the format expected from
// scraping the unity showcase database)
func getGameList(html string) []string {
  list := make([]string, 0)
  splitByGame := strings.Split(html, "<li class=\"game")
  for i := 1; i < len(splitByGame); i++ {
    game := splitByGame[i]
    title := getFirstBetween(game, "<h3 class=\"mb10 title\">", "</h3>")
    developer := strings.Split(getFirstBetween(game, "<p class=\"mb0\"><a target=\"_blank\" class=\"developer\" href=\"", "</a></p>"), "\">")
    developerName := developer[1]
    developerURL := developer[0]
    imageURL := getFirstBetween(game, "<div class=\"image\" style=\"background-image: url(", ");\"></div>")
    description := getFirstBetween(game, "<div class=\"mb15 description clear\"><p>", "</p>")

   list = append(list, makeWebPage(title, developerName, developerURL, imageURL, description))
  }

  return list
}

// Returns the substring of "whole" that begins after the first instance of
// "before" and ends before the first occurrence of "after" after that
// occurrence of "before". panics if "whole" does not contain "before".
func getFirstBetween(whole string, before string, after string) string {
  return strings.Split(strings.Split(whole, before)[1], after)[0]
}

// returns a string of a basic HTML page that displays the title as a header,
// the developer name as a link to the developer url, the image given by the
// image url, and the description in paragraphc format at the bottom.
// The title of the webpage is the title of the game.
func makeWebPage(title string, developerName string, developerURL string, imageURL string, description string) string {
  html := "<!DOCTYPE html>"
  html += "<html>"
  html += "<head>"
  html += "<title>" + title + "</title>"
  html += "</head>"
  html += "<body>"

  html += "<h1>" + title + "</h1>"
  html += "<img src=\"" + imageURL + "\"><br>"
  html += "<a href=\"" + developerURL+ "\">" + developerName + "</a><br>"
  html += "<p>" + description + "</p>"

  html += "</body>"
  html += "</html>"

  return html
}

// panics with the string representation of the error 
// if the given error is not nil
func checkErr(err error) {
  if (err != nil) {
    panic(err)
  }
}